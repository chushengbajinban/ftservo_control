cmake_minimum_required(VERSION 3.0.2)
project(ftservo_control)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++14)
set(CMAKE_CXX_FLAGS_RELEASE "-O3 -Wall -g")

find_package(Eigen3 REQUIRED)

find_package(catkin REQUIRED COMPONENTS
  geometry_msgs
  roscpp
  rospy
  std_msgs
  tf
)

catkin_package(
)

include_directories(
# include
  SCServo_Linux
  ${catkin_INCLUDE_DIRS}
  ${EIGEN3_INCLUDE_DIR}
  )

file(GLOB SCServo SCServo_Linux/*.h SCServo_Linux/*.cpp)

add_executable(single_servo_read_write src/single_servo_read_write.cpp ${SCServo}) 
target_link_libraries(single_servo_read_write ${catkin_LIBRARIES})
add_dependencies(single_servo_read_write ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

add_executable(fake_servo src/fake_servo.cpp ${SCServo}) 
target_link_libraries(fake_servo ${catkin_LIBRARIES})
